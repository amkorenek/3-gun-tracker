package com.example.ashton.a3guntracker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class NewMatch extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_match);
        Button timePage = (Button) findViewById(R.id.btnTime);
        timePage.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view){
                Intent intent = new Intent(NewMatch.this, TimePage.class);
                startActivity(intent);
            }
        });

        Button gunsPage = (Button) findViewById(R.id.btnGuns);
        gunsPage.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view){
                Intent intent = new Intent(NewMatch.this, GunsPage.class);
                startActivity(intent);
            }
        });

        Button stagePage = (Button) findViewById(R.id.btnStage);
        stagePage.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view){
                Intent intent = new Intent(NewMatch.this, StageNotesPage.class);
                startActivity(intent);
            }
        });
        }
}
