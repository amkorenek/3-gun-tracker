package com.example.ashton.a3guntracker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Match extends AppCompatActivity{

    double timeStage1, timeStage2, timeStage3, timeStage4, timeStage5;
    double penStg1, penStg2, penStg3, penStg4, penStg5;
    double stgPar1, stgPar2, stgPar3, stgPar4, stgPar5;
    String pisMake, pisModel, pisCal, pisAmmo, pisNotes;
    String rifMake, rifModel, rifCal, rifAmmo, rifNotes;
    String sgMake, sgModel, sgCal, sgAmmo, sgNotes;
    String pcMake, pcModel, psCal, pcAmmo, pcNotes;
    String stg1Notes, stg2Notes, stg3Notes, stg4Notes, stg5Notes;

    public Match() {
    }

    public void pistolUpdate(){

        Button gunsPage = (Button) findViewById(R.id.pistolSave);
        gunsPage.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view){
                pisMake = findViewById(R.id.pistolMake).toString();
                Log.d(pisMake, "hello");
            }
        });
    }
}
