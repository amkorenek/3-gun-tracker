package com.example.ashton.a3guntracker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class GunsPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guns_page);

        Button pistolPage = (Button) findViewById(R.id.btnPistol);
        pistolPage.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view){
                Intent intent = new Intent(GunsPage.this, PistolPage.class);
                startActivity(intent);
            }
        });

        Button riflePage = (Button) findViewById(R.id.btnRifle);
        riflePage.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view){
                Intent intent = new Intent(GunsPage.this, RiflePage.class);
                startActivity(intent);
            }
        });

        Button shotgunPage = (Button) findViewById(R.id.btnShotgun);
        shotgunPage.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view){
                Intent intent = new Intent(GunsPage.this, ShotgunPage.class);
                startActivity(intent);
            }
        });

        Button pccPage = (Button) findViewById(R.id.btnPCC);
        pccPage.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view){
                Intent intent = new Intent(GunsPage.this, PccPage.class);
                startActivity(intent);
            }
        });
    }
}
