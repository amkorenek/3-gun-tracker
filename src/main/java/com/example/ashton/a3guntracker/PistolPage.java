package com.example.ashton.a3guntracker;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class PistolPage extends AppCompatActivity{

    String pisMake, pisModel, pisCal, pisAmmo, pisNotes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pistol_page);

        Button savePage = (Button) findViewById(R.id.pistolSave);
        savePage.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view){
                pisMake = findViewById(R.id.pistolMake).toString();
                pisModel = findViewById(R.id.pistolModel).toString();
                pisCal = findViewById(R.id.pistolCal).toString();
                pisAmmo = findViewById(R.id.pistolAmmo).toString();
                pisNotes = findViewById(R.id.pistolNotes).toString();
                try {
                    PistolSave();
                }catch (Throwable t){
                    Toast.makeText(getApplicationContext(), "Exception: " + t.toString(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void PistolSave() {
        try{
            FileWriter fWriter = new FileWriter("/data/data/a3guntracker/files/holder.txt");
            PrintWriter pWriter = new PrintWriter(fWriter);
            pWriter.println(pisMake);
            pWriter.println(pisModel);
            pWriter.println(pisCal);
            pWriter.println(pisAmmo);
            pWriter.println(pisNotes);
            pWriter.close();
        }catch (Throwable t) {
             Toast.makeText(this, "Exception: " + t.toString(), Toast.LENGTH_LONG).show();
        }
    }
}
